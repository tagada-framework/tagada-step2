#!/usr/bin/python3

import argparse
import os

import yaml

import shell
from shell import Bin
from utils import eprint, download_and_execute, set_executable


def install_sdkman():
    eprint("Installing sdk")
    download_and_execute("https://get.sdkman.io", "sdkman.io")
    set_executable("sdk")


def install_java(dependencies):
    eprint(f"Installing java {dependencies['java']}")
    sdk.install("java", dependencies["java"])


def install_gradle(dependencies):
    eprint(f"Installing gradle {dependencies['gradle']}")
    sdk.install("gradle", dependencies["gradle"])


def generate_env(dependencies):
    env_sh = f"""
    #!/usr/bin/bash
    # Step2
    ## Add custom java to path
    export PATH="$HOME/.sdkman/candidates/java/{dependencies['java']}/bin:$PATH"

    ## Add custom gradle to path
    export PATH="$HOME/.sdkman/candidates/gradle/{dependencies['gradle']}/bin:$PATH"
    export STEP2="$TAGADA_HOME/tagada-step2/build/libs/tagada-step2-1.0.0-all.jar"
    """
    with open("env.sh", "w") as env_sh_file:
        env_sh_file.write(env_sh.strip())


if __name__ == "__main__":
    parser = argparse.ArgumentParser("install.py")
    parser.add_argument("-v", "--verbose", help="enable verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        shell.VERBOSITY = True

    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
    dependencies = config["dependencies"]
    # Install SDKMan
    install_sdkman()
    sdk = Bin(os.path.realpath("sdk"))
    sdk.install = sdk % "install"
    # Install java
    install_java(dependencies)
    # Build Tagada Step2
    gradlew = Bin("./gradlew")
    gradlew.shadow_jar = gradlew % "shadowJar"
    gradlew.shadow_jar()
    # Generate env.sh file
    generate_env(dependencies)
