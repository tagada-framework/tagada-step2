package crypto

class GF private constructor(
    private val poly: Int
) {

    companion object {
        private val CACHED_FIELDS = mutableMapOf<Int, GF>()
        fun of(poly: Int): GF = CACHED_FIELDS.getOrPut(poly) { GF(poly) }
    }

    private val nbBits = Int.SIZE_BITS - 1 - poly.countLeadingZeroBits()
    private val inv = IntArray(1 shl nbBits)

    init {
        assert(poly > 0)
        for (x in 1 until (1 shl nbBits)) {
            for (y in 1 until (1 shl nbBits)) {
                if (times(y, x) == 1) {
                    inv[x] = y
                    break
                }
            }
        }
    }

    fun times(lhs: Int, rhs: Int): Int {
        var p = 0
        val highBit = 1 shl (nbBits - 1)
        var a = lhs
        var b = rhs
        for (_x in 0 until nbBits) {
            if ((b and 1) != 0) {
                p = p xor a
            }
            val highBitSet = (a and highBit) != 0;
            a = a shl 1;
            if (highBitSet) {
                a = a xor poly
            }
            b = b shr 1
        }
        return p
    }

    fun div(lhs: Int, rhs: Int): Int =
        if (rhs == 0) throw ArithmeticException("/ by zero.")
        else times(lhs, inv[rhs])


    fun inverse(matrix: Array<IntArray>): Array<IntArray> {
        require (matrix.size == matrix[0].size) { "invalid matrix dimensions for inverse: M != N." }
        val M = Array(matrix.size) { i -> IntArray(matrix[i].size) { j -> matrix[i][j] } }
        val I = Array(matrix.size) { i -> IntArray(matrix[i].size) { j -> if (i == j) 1 else 0 } }

        for (pivot in 0 until M[0].size) {
            var row = pivot
            while (row < M.size && M[row][pivot] == 0) {
                row += 1
            }
            if (row == M.size) throw ArithmeticException("matrix not invertible.")

            if (row == pivot) {
                val coefficient = M[pivot][pivot]
                if (coefficient != 1) {
                    for (k in 0 until M[0].size) {
                        M[pivot][k] = div(M[pivot][k], coefficient)
                        I[pivot][k] = div(I[pivot][k], coefficient)
                    }
                }
            } else {
                val coefficient = M[row][pivot]
                if (coefficient == 1) {
                    for (k in 0 until M[0].size) {
                        M[pivot][k] = M[pivot][k] xor M[row][k]
                        I[pivot][k] = I[pivot][k] xor I[row][k]
                    }
                } else {
                    for (k in 0 until M[0].size) {
                        M[pivot][k] = M[pivot][k] xor div(M[row][k], coefficient)
                        I[pivot][k] = I[pivot][k] xor div(I[row][k], coefficient)
                    }
                }
            }

            for (@Suppress("NAME_SHADOWING") row in 0 until M.size) {
                if (row != pivot && M[row][pivot] != 0) {
                    val coefficient = M[row][pivot]
                    if (coefficient == 1) {
                        for (k in 0 until M[0].size) {
                            M[row][k] = M[row][k] xor M[pivot][k]
                            I[row][k] = I[row][k] xor I[pivot][k]
                        }
                    } else {
                        for (k in 0 until M[0].size) {
                            M[row][k] = M[row][k] xor times(M[pivot][k], coefficient)
                            I[row][k] = I[row][k] xor times(I[pivot][k], coefficient)
                        }
                    }
                }
            }
        }
        return I
    }
}