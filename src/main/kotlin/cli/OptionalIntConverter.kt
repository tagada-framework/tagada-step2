package cli

import picocli.CommandLine
import java.util.*

class OptionalIntConverter : CommandLine.ITypeConverter<OptionalInt> {
    override fun convert(value: String): OptionalInt {
        if (value == "null") return OptionalInt.empty()
        return OptionalInt.of(Integer.valueOf(value))
    }
}