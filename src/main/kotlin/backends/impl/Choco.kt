package backends.impl

import algo.cartesianProduct
import backends.api.ModelBuilder
import backends.api.SBox
import backends.impl.choco.propagators.gac.PropGMult
import backends.impl.choco.propagators.gac.PropLFSR
import backends.impl.choco.propagators.gac.PropXOR3
import backends.impl.choco.propagators.gac.PropXORN
import backends.impl.choco.valueselector.BestSBoxTransition
import fr.limos.gitlab.decrypt.tagada.structs.common.Value
import fr.limos.gitlab.decrypt.tagada.structs.specification.*
import org.chocosolver.cutoffseq.LubyCutoffStrategy
import org.chocosolver.solver.Model
import org.chocosolver.solver.constraints.Constraint
import org.chocosolver.solver.constraints.extension.Tuples
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution
import org.chocosolver.solver.search.strategy.Search.*
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef
import org.chocosolver.solver.search.strategy.strategy.IntStrategy
import org.chocosolver.solver.variables.IntVar
import utils.SolvingRace

class Choco : ModelBuilder<IntVar>() {

    private val model = Model()
    override fun IntVar.contains(value: Int) = this.contains(value)

    override fun newIntVar(values: IntArray): IntVar = model.intVar(values)
    override fun newIntVar(min: Int, maxExclusive: Int): IntVar = model.intVar(min, maxExclusive - 1)
    override fun newConstant(value: Int): IntVar = model.intVar(value)
    override fun postTable(inputs: List<IntVar>, op: Table, outputs: List<IntVar>) {
        require(op.tuples.all { it.size == inputs.size + outputs.size })
        model.table((inputs + outputs).toTypedArray(), Tuples(op.tuples.map { it.toIntArray() }.toTypedArray(), true))
            .post()
    }

    override fun postEqual(inputs: List<IntVar>, op: Equal, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        inputs[0].eq(outputs[0]).post()
    }

    private fun xorDomains(lhs: IntVar, rhs: IntVar): IntArray {
        val domain = mutableSetOf<Int>()
        var lhsVal = lhs.lb
        val lhsUB = lhs.ub
        while (lhsVal < lhsUB) {
            var rhsVal = rhs.lb
            val rhsUB = rhs.ub
            while (rhsVal < rhsUB) {
                domain += lhsVal xor rhsVal
                rhsVal = rhs.nextValue(rhsVal)
            }
            lhsVal = lhs.nextValue(lhsVal)
        }
        return domain.toIntArray()
    }

    override fun postBitwiseXOR(inputs: List<IntVar>, op: BitwiseXOR, outputs: List<IntVar>) {
        val variables = (inputs + outputs).toTypedArray()
        when (variables.size) {
            0 -> {}
            1 -> variables[0].eq(0).post()
            2 -> variables[0].eq(variables[1]).post()
            3 -> model.post(Constraint("TernaryXOR", PropXOR3(variables[0], variables[1], variables[2])))
            4 -> {
                val tmp0 = newIntVar(xorDomains(variables[0], variables[1]))
                model.post(Constraint("TernaryXOR", PropXOR3(variables[0], variables[1], tmp0)))
                model.post(Constraint("TernaryXOR", PropXOR3(variables[2], variables[3], tmp0)))
            }
            5 -> {
                val tmp0 = newIntVar(xorDomains(variables[0], variables[1]))
                model.post(Constraint("TernaryXOR", PropXOR3(variables[0], variables[1], tmp0)))
                val tmp1 = newIntVar(xorDomains(variables[2], variables[3]))
                model.post(Constraint("TernaryXOR", PropXOR3(variables[2], variables[3], tmp1)))
                model.post(Constraint("TernaryXOR", PropXOR3(tmp0, tmp1, variables[4])))
            }
            else -> model.post(Constraint("NAryXOR", PropXORN(variables)))
        }
    }

    override fun postGFMul(inputs: List<IntVar>, op: GFMul, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        model.post(
            Constraint(
                "GFMul",
                PropGMult(op.poly, inputs[0], op.constant, outputs[0])
            )
        )
    }

    override fun postCommand(inputs: List<IntVar>, op: Command, outputs: List<IntVar>) {
        TODO("Not yet implemented")
    }

    override fun postLFSR(inputs: List<IntVar>, op: LFSR, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        model.post(Constraint("LFSR", PropLFSR.of(inputs[0], outputs[0], op)))
    }

    override fun postSplit(inputs: List<IntVar>, op: Split, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == op.toWords.size)
        fun splitInto(toWords: List<List<Int>>, word: Value, output: IntArray) {
            for (i in 0 until toWords.size) {
                output[i + 1] = 0
            }
            for ((i, wordBits) in toWords.withIndex()) {
                for (bit in wordBits) {
                    output[i + 1] = output[i + 1] shl 1
                    output[i + 1] += if (word and (1 shl bit) != 0) { 1 } else { 0 }
                }
            }
        }
        val tuples = Array(inputs[0].ub + 1) { x ->
            val tuple = IntArray(inputs.size + outputs.size)
            tuple[0] = x
            splitInto(op.toWords, x, tuple)
            tuple
        }
        model.table((inputs + outputs).toTypedArray(), Tuples(tuples, true)).post()
    }

    override fun postConcat(inputs: List<IntVar>, op: Concat, outputs: List<IntVar>) {
        fun concatInto(toWord: List<BitAndWord>, words: IntArray, output: IntArray) {
            var word = 0

            for ((bitPos, wordPos) in toWord) {
                word = word shl 1
                word += if (words[wordPos] and (1 shl bitPos) != 0) 1 else 0
            }
            output[output.size - 1] = word
        }
        val tuples = mutableListOf<IntArray>()
        cartesianProduct( inputs.map { 0 .. it.ub } ) { inputValues ->
            val tuple = IntArray(inputValues.size + 1)
            for (i in 0 until inputValues.size) {
                tuple[i] = inputValues[i]
            }
            concatInto(op.toWord, inputValues, tuple)
            tuples += tuple
        }
        model.table((inputs + outputs).toTypedArray(), Tuples(tuples.toTypedArray(), true)).post()
    }

    override fun postShift(inputs: List<IntVar>, op: Shift, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        val mask = ((1 shl op.nbBits) - 1)
        val tuples = when (op.direction) {
            Direction.ShiftLeft -> Array( inputs[0].ub + 1 ) { x -> intArrayOf(x, (x shl op.bitCount) and mask) }
            Direction.ShiftRight -> Array( inputs[0].ub + 1 ) { x -> intArrayOf(x, (x shr op.bitCount) and mask) }
        }
        model.table(inputs[0], outputs[0], Tuples(tuples, true)).post()
    }

    override fun postCircularShift(inputs: List<IntVar>, op: CircularShift, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        val mask = ((1 shl op.nbBits) - 1)
        val tuples = when (op.direction) {
            Direction.ShiftLeft -> Array( inputs[0].ub + 1 ) { x -> intArrayOf(x, ((x shl op.bitCount) or (x shr (op.nbBits - op.bitCount))) and mask) }
            Direction.ShiftRight -> Array( inputs[0].ub + 1 ) { x -> intArrayOf(x, ((x shr op.bitCount) or (x shl (op.nbBits - op.bitCount))) and mask) }
        }
        model.table(inputs[0], outputs[0], Tuples(tuples, true)).post()
    }

    override fun postBitwiseAndConst(inputs: List<IntVar>, op: AndConst, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        val tuples = Array( inputs[0].ub + 1 ) { x -> intArrayOf(x, x and op.constant) }
        model.table(inputs[0], outputs[0], Tuples(tuples, true)).post()
    }

    override fun postBitwiseOrConst(inputs: List<IntVar>, op: OrConst, outputs: List<IntVar>) {
        require(inputs.size == 1)
        require(outputs.size == 1)
        val tuples = Array( inputs[0].ub + 1 ) { x -> intArrayOf(x, x or op.constant) }
        model.table(inputs[0], outputs[0], Tuples(tuples, true)).post()
    }

    override fun sumSet(vars: List<IntVar>): IntVar {
        val lb = vars.sumOf { it.lb }
        val ub = vars.sumOf { it.ub }
        val sum = model.intVar(lb, ub, true)
        model.sum(vars.toTypedArray(), "=", sum).post()
        return sum
    }

    override fun minimize(obj: IntVar): Map<IntVar, Value>? {
        model.setObjective(false, obj)
        val solver = model.solver
        val valueSelector = BestSBoxTransition(sBoxes, bestTransition, bestInvTransition, IntDomainMin(), false)
        val inlineSBoxes = sBoxes.flatMap(SBox<IntVar>::toList).distinct()
            .toTypedArray()
        val searchSteps = mutableListOf<IntStrategy>()
        if (nullVariables.isNotEmpty()) searchSteps += inputOrderLBSearch(*nullVariables.toTypedArray())
        searchSteps += IntStrategy(inlineSBoxes, DomOverWDegRef(inlineSBoxes, 0), valueSelector)
        solver.setSearch(lastConflict(sequencer(*searchSteps.toTypedArray()), 4))
        solver.makeCompleteStrategy(true)
        var solution: Map<IntVar, Value>? = null
        while (solver.solve()) {
            solution = model.retrieveIntVars(true)
                .associateWith(IntVar::getValue)
        }
        return solution
    }

    override fun addModeltoRace(track: SolvingRace, buffer: StringBuffer, obj: IntVar){
        model.setObjective(false, obj)
        val solver = model.solver
        val valueSelector = BestSBoxTransition(sBoxes, bestTransition, bestInvTransition, IntDomainMin(), false)
        val inlineSBoxes = sBoxes.flatMap(SBox<IntVar>::toList).distinct()
            .toTypedArray()
        val searchSteps = mutableListOf<IntStrategy>()
        if (nullVariables.isNotEmpty()) searchSteps += inputOrderLBSearch(*nullVariables.toTypedArray())
        searchSteps += IntStrategy(inlineSBoxes, DomOverWDegRef(inlineSBoxes, 0), valueSelector)
        solver.setSearch(lastConflict(sequencer(*searchSteps.toTypedArray()), 4))
        solver.makeCompleteStrategy(true)

        solver.setRestarts({ l: Long -> solver.failCount >= l }, LubyCutoffStrategy(2), 256)//TODO fine tune the restartsLimit parameter ?
        solver.setNoGoodRecordingFromRestarts()
        solver.showShortStatistics()

        model.addHook("PRINTER", IMonitorSolution {
            buffer.append("OBJ =").append(obj.value).append("\n")
            buffer.append(model.retrieveIntVars(true).associateWith(IntVar::getValue))
        } as IMonitorSolution?)

        track.addModel {
            model
        }
    }

    override fun lessThan(variable: IntVar, value: Int) {
        variable.lt(value).post()
    }

    override fun greaterThan(variable: IntVar, value: Int) {
        variable.gt(value).post()
    }
}