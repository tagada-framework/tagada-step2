package backends.impl.choco.valueselector

import backends.api.SBox
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap
import it.unimi.dsi.fastutil.ints.IntArraySet
import org.chocosolver.solver.search.strategy.selectors.values.IntValueSelector
import org.chocosolver.solver.variables.IntVar

class BestSBoxTransition(
    sboxes: List<SBox<IntVar>>,
    val optimalSBoxTransition: Map<IntVar, IntArray>,
    val optimalInvSBoxTransition: Map<IntVar, IntArray>,
    val fallbackStrategy: IntValueSelector,
    val maximize: Boolean
) : IntValueSelector {

    val sBoxOf = Int2ObjectArrayMap<IntVar>()
    val invSBoxOf = Int2ObjectArrayMap<IntVar>()
    val probabilities = IntArraySet()

    init {
        for ((δin, δout, p) in sboxes) {
            sBoxOf[δin.id] = δout
            invSBoxOf[δout.id] = δin
            probabilities.add(p.id)
        }
    }

    override fun selectValue(variable: IntVar): Int {
        if (variable.id in probabilities) {
            return if(maximize) variable.ub else variable.lb
        }

        val sx = sBoxOf[variable.id]
        if (sx != null) {
            if (sx.isInstantiated) {
                val sxValue = sx.value
                val bestTransition = optimalSBoxTransition[variable]!![sxValue]
                if (bestTransition in variable) {
                    return bestTransition
                }
            }
        }

        val x = invSBoxOf[variable.id]
        if (x != null) {
            if (x.isInstantiated) {
                val xValue = x.value
                val bestTransition = optimalInvSBoxTransition[variable]!![xValue]
                if (bestTransition in variable) {
                    return bestTransition
                }
            }
        }

        return fallbackStrategy.selectValue(variable)
    }
}