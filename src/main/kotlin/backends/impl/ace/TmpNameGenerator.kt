package backends.impl.ace

class TmpNameGenerator() {
    private var i = 0
    fun next(): String {
        val name = "tmp_$i"
        i += 1
        return name
    }
}