package backends.api

data class SBox<Var>(val dIn: Var, val dOut: Var, val p: Var) {
    fun toList() = listOf(dIn, dOut, p)
}