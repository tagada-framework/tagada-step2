package backends.api

import fr.limos.gitlab.decrypt.tagada.structs.differential.DDT
import fr.limos.gitlab.decrypt.tagada.structs.differential.DifferentialStateNode
import fr.limos.gitlab.decrypt.tagada.structs.differential.DifferentialStateNodeUID
import fr.limos.gitlab.decrypt.tagada.structs.specification.*
import utils.SolvingRace

interface IModelBuilder<Var> {
    operator fun Var.contains(value: Int): Boolean

    fun const(value: Int): Var
    fun state(uid: DifferentialStateNodeUID, abstraction: Boolean?, insert: () -> DifferentialStateNode): Var

    fun postAndReturn(inputs: List<Var>, op: DDT, outputs: List<Var>): SBox<Var>

    fun postEqual(inputs: List<Var>, op: Equal, outputs: List<Var>)

    fun postBitwiseXOR(inputs: List<Var>, op: BitwiseXOR, outputs: List<Var>)

    fun postTable(inputs: List<Var>, op: Table, outputs: List<Var>)

    fun postCommand(inputs: List<Var>, op: Command, outputs: List<Var>)

    fun postGFMul(inputs: List<Var>, op: GFMul, outputs: List<Var>)

    fun postGFMatToVecMul(inputs: List<Var>, op: GFMatToVecMul, outputs: List<Var>)

    fun postLFSR(inputs: List<Var>, op: LFSR, outputs: List<Var>)

    fun postSplit(inputs: List<Var>, op: Split, outputs: List<Var>)

    fun postConcat(inputs: List<Var>, op: Concat, outputs: List<Var>)

    fun postShift(inputs: List<Var>, op: Shift, outputs: List<Var>)

    fun postCircularShift(inputs: List<Var>, op: CircularShift, outputs: List<Var>)

    fun postBitwiseAndConst(inputs: List<Var>, op: AndConst, outputs: List<Var>)

    fun postBitwiseOrConst(inputs: List<Var>, op: OrConst, outputs: List<Var>)

    fun sumSet(vars: List<Var>): Var
    fun minimize(obj: Var, names: Map<String, DifferentialStateNodeUID>): Assignment?
    fun addModeltoRace(track: SolvingRace, buffer: StringBuffer, obj: Var)

    fun lessThan(variable: Var, value: Int)
    fun greaterThan(variable: Var, value: Int)
}