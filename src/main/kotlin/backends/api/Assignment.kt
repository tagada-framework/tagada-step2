package backends.api

import fr.limos.gitlab.decrypt.tagada.structs.common.Value

typealias Assignment = Map<String, Value>