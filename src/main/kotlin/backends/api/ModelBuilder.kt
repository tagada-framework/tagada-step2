package backends.api

import crypto.GF
import fr.limos.gitlab.decrypt.tagada.structs.common.Range
import fr.limos.gitlab.decrypt.tagada.structs.common.Sparse
import fr.limos.gitlab.decrypt.tagada.structs.common.Value
import fr.limos.gitlab.decrypt.tagada.structs.differential.*
import fr.limos.gitlab.decrypt.tagada.structs.differential.Constant
import fr.limos.gitlab.decrypt.tagada.structs.differential.Variable
import fr.limos.gitlab.decrypt.tagada.structs.specification.*
import utils.SolvingRace
import utils.log2
import java.lang.Integer.max

abstract class ModelBuilder<Var> : IModelBuilder<Var> {

    private val _namedStates = mutableMapOf<DifferentialStateNodeUID, Var>()
    private val constants = mutableMapOf<Int, Var>()

    protected val namedStates: Map<DifferentialStateNodeUID, Var> = _namedStates
    protected val bestTransition = mutableMapOf<Var, IntArray>()
    protected val bestInvTransition = mutableMapOf<Var, IntArray>()
    protected val sBoxes = mutableListOf<SBox<Var>>()
    protected val nullVariables = mutableListOf<Var>()

    protected abstract fun newConstant(value: Int): Var
    protected abstract fun newIntVar(values: IntArray): Var
    protected abstract fun newIntVar(min: Int, maxExclusive: Int): Var
    private fun newTmpVar(nbBits: Int) = newIntVar(0, (1 shl nbBits) - 1)

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun newProbaVar(possibleProbabilities: Set<MinusLog2Probability>): Var =
        newIntVar(possibleProbabilities.map { it.hundredth.value }.sorted().toIntArray())

    override fun const(value: Int) = constants.getOrPut(value) { newConstant(value) }
    override fun state(uid: DifferentialStateNodeUID, abstraction: Boolean?, insert: () -> DifferentialStateNode): Var =
        _namedStates.getOrPut(uid) {
            when (val stateNode = insert()) {
                is Variable -> {
                    when (val domain = stateNode.domain) {
                        is Range -> when (abstraction) {
                            true -> newIntVar(max(domain.min, 1), domain.max)
                            false -> const(0)
                            else -> newIntVar(domain.min, domain.max)
                        }

                        is Sparse -> when (abstraction) {
                            true -> newIntVar(domain.values.sorted().filter { it > 0 }.toIntArray())
                            false -> const(0)
                            else -> newIntVar(domain.values.sorted().toIntArray())
                        }
                    }
                }

                is Constant -> const(stateNode.value)
                is Probability -> newIntVar(stateNode.domain.map { it.hundredth.value }.toIntArray())
            }
        }

    override fun postAndReturn(inputs: List<Var>, op: DDT, outputs: List<Var>): SBox<Var> {
        require(inputs.size == 1)
        require(outputs.size == 1)
        val validProbabilities = mutableSetOf<MinusLog2Probability>()
        val nbDeltaIn = op.probabilities.keys.maxOf(DDTArgs::deltaIn) + 1
        val nbDeltaOut = op.probabilities.keys.maxOf(DDTArgs::deltaOut) + 1
        val bestProbability = IntArray(nbDeltaIn) { Int.MAX_VALUE }
        val bestTransition = IntArray(nbDeltaIn) { -1 }
        val bestInvProbability = IntArray(nbDeltaOut) { Int.MAX_VALUE }
        val bestInvTransition = IntArray(nbDeltaOut) { -1 }
        val tuples = op.probabilities
            .filter { (args, _) -> (args.deltaIn in inputs[0] && args.deltaOut in outputs[0]) }
            .map { (args, p) ->
                val (dIn, dOut) = args
                validProbabilities += p
                if (bestProbability[dIn] > p.hundredth.value) {
                    bestProbability[dIn] = p.hundredth.value
                    bestTransition[dIn] = dOut
                }
                if (bestInvProbability[dOut] > p.hundredth.value) {
                    bestInvProbability[dOut] = p.hundredth.value
                    bestInvTransition[dOut] = dIn
                }
                listOf(dIn, dOut, p.hundredth.value)
            }
        val sBox = SBox(inputs[0], outputs[0], newProbaVar(validProbabilities))
        this.bestTransition[sBox.dIn] = bestTransition
        this.bestInvTransition[sBox.dOut] = bestInvTransition
        postTable(sBox.toList(), Table(tuples), emptyList())
        sBoxes += sBox
        return sBox
    }

    override fun postGFMatToVecMul(inputs: List<Var>, op: GFMatToVecMul, outputs: List<Var>) {
        postDecomposition(inputs, op, outputs)
        val inverseMatrix = try {
            GF.of(op.poly).inverse(op.m.map { it.toIntArray() }.toTypedArray())
        } catch (e: ArithmeticException) {
            null
        }
        if (inverseMatrix != null) {
            postDecomposition(outputs, op.copy(m = inverseMatrix.map { it.toList() }), inputs)
        }
    }

    private fun postDecomposition(inputs: List<Var>, op: GFMatToVecMul, outputs: List<Var>) {
        for (j in 0 until outputs.size) {
            val vars = mutableListOf<Var>()
            for (k in 0 until inputs.size) {
                when (op.m[j][k]) {
                    0 -> {}
                    1 -> vars += inputs[k]
                    else -> {
                        val mulResult = newTmpVar(log2(op.poly))
                        postGFMul(listOf(inputs[k]), GFMul(op.poly, op.m[j][k]), listOf(mulResult))
                        vars += mulResult
                    }
                }
            }
            postBitwiseXOR(vars, BitwiseXOR, listOf(outputs[j]))
        }
    }

    override fun minimize(
        obj: Var,
        names: Map<String, DifferentialStateNodeUID>
    ): Assignment? {
        val valueOf = minimize(obj) ?: return null
        val assignment = names.entries.associateTo(mutableMapOf()) { (name, uid) ->
            name to valueOf[namedStates[uid]!!]!!
        }
        assignment["obj"] = valueOf[obj]!!
        return assignment
    }

    abstract fun minimize(obj: Var): Map<Var, Value>?

    override fun addModeltoRace(track: SolvingRace, buffer: StringBuffer, obj: Var) {
        addModeltoRace(track, buffer, obj)
    }
}