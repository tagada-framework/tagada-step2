package backends.api

import fr.limos.gitlab.decrypt.tagada.structs.common.Value

typealias AssignmentList = List<Assignment>
/*JSON example
[{
"in_0": 0,
"in_1": 1,
"in_2": 0
}, {
"in_0": 0,
"in_1": 0,
"in_2": 1
}]
*/