package algo

import backends.api.*
import backends.api.SBox
import utils.SolvingRace
import fr.limos.gitlab.decrypt.tagada.structs.differential.DDT
import fr.limos.gitlab.decrypt.tagada.structs.differential.DifferentialGraph
import fr.limos.gitlab.decrypt.tagada.structs.differential.DifferentialStateNodeUID
import fr.limos.gitlab.decrypt.tagada.structs.differential.Linear
import fr.limos.gitlab.decrypt.tagada.structs.specification.*
import utils.panic
import java.util.*


@Suppress("NAME_SHADOWING")
fun <Var> raceBoundedOptimalDifferentialCharacteristic(
    track: SolvingRace,
    buffer: StringBuffer,
    graph: DifferentialGraph,
    s1: Assignment,
    lb: OptionalInt,
    builder: IModelBuilder<Var>
){
    val sBoxes = mutableListOf<SBox<Var>>()
    val abstraction = mutableMapOf<DifferentialStateNodeUID, Boolean>()
    for ((name, value) in s1) {
        if (name.startsWith("in_")) {
            val variable = graph.names[name] ?: panic("variable $name is not present in the graph")
            require(value == 0 || value == 1) { "cannot reify δ$name with Δ$name = $value. The value of Δ$name must be either 0 or 1." }
            abstraction[variable] = (value > 0)
        }
    }
    for ((inputs, function, outputs) in graph.transitions) {
        val outputs = outputs.map { builder.state(it, abstraction[it]) { graph.states[it.uid] } }
        val inputs = inputs.map { builder.state(it, abstraction[it]) { graph.states[it.uid] } }
        val (_, _, function) = graph.functions[function.uid]
        when (function) {
            is DDT -> sBoxes += builder.postAndReturn(inputs, function, outputs)
            is Linear -> {
                when (val function = function.linear) {
                    is Equal -> builder.postEqual(inputs, function, outputs)
                    is BitwiseXOR -> builder.postBitwiseXOR(inputs, function, outputs)
                    is Table -> builder.postTable(inputs, function, outputs)
                    is Command -> builder.postCommand(inputs, function, outputs)
                    is GFMul -> builder.postGFMul(inputs, function, outputs)
                    is GFMatToVecMul -> builder.postGFMatToVecMul(inputs, function, outputs)
                    is LFSR -> builder.postLFSR(inputs, function, outputs)
                    is Split -> builder.postSplit(inputs, function, outputs)
                    is Concat -> builder.postConcat(inputs, function, outputs)
                    is Shift -> builder.postShift(inputs, function, outputs)
                    is CircularShift -> builder.postCircularShift(inputs, function, outputs)
                    is AndConst -> builder.postBitwiseAndConst(inputs, function, outputs)
                    is OrConst -> builder.postBitwiseOrConst(inputs, function, outputs)
                }
            }
        }
    }
    val inputDifferences = builder.sumSet((graph.plaintext + graph.key)
        .map { builder.state(it, abstraction[it]) { graph.states[it.uid] } }
    )
    builder.greaterThan(inputDifferences, 0)
    val obj = builder.sumSet(sBoxes.map { it.p })
    lb.ifPresent { builder.lessThan(obj, it) }
    builder.addModeltoRace(track, buffer, obj)
}