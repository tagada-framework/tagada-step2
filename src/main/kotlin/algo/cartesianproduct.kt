package algo

fun cartesianProduct(iterators: List<Iterable<Int>>, onEach: (IntArray) -> Unit) =
    innerCartesianProduct(iterators, 0, IntArray(iterators.size), onEach)

private fun innerCartesianProduct(
    iterators: List<Iterable<Int>>,
    depth: Int,
    arr: IntArray,
    onEach: (IntArray) -> Unit
) {
    if (depth == iterators.size) {
        onEach(arr)
    } else {
        for (value in iterators[depth]) {
            arr[depth] = value
            innerCartesianProduct(iterators, depth + 1, arr, onEach)
        }
    }
}