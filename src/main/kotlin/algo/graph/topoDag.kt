package algo.graph

import fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential.NonLinearTransition
import fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential.TruncatedDifferentialGraph
import fr.limos.gitlab.decrypt.tagada.structs.truncateddifferential.TruncatedDifferentialTransition

fun topoDag(g: TruncatedDifferentialGraph): MutableList<TruncatedDifferentialTransition> {
    val visited = mutableSetOf<TruncatedDifferentialTransition>()
    val orderedTransitions = ArrayDeque<TruncatedDifferentialTransition>()

    TODO()
}

fun getFreeParameters(g: TruncatedDifferentialGraph): Set<TruncatedDifferentialTransition> {

    val transitions = topoDag(g)

    val isFree = (g.plaintext + g.key + g.ciphertext).toMutableSet()

    val unposted = mutableSetOf<TruncatedDifferentialTransition>()
    for ((inputs, function, outputs) in transitions) {

        val f = g.functions[function.uid].function
        if (inputs.all(isFree::contains) && f !is NonLinearTransition) {
            isFree.addAll(outputs)
            unposted.add(TruncatedDifferentialTransition(inputs, function, outputs))
        }

    }
    for ((inputs, function, outputs) in transitions.reversed()) {
        val f = g.functions[function.uid].function
        if (outputs.all(isFree::contains) && f !is NonLinearTransition) {
            isFree.addAll(inputs)
            unposted.add(TruncatedDifferentialTransition(inputs, function, outputs))
        }
    }

    return unposted
}