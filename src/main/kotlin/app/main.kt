package app

import algo.boundedOptimalDifferentialCharacteristic
import algo.raceBoundedOptimalDifferentialCharacteristic
import backends.Backend
import backends.api.Assignment
import backends.api.AssignmentList
import backends.impl.Choco
import cli.OptionalIntConverter
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import fr.limos.gitlab.decrypt.tagada.structs.api.Differential
import fr.limos.gitlab.decrypt.tagada.structs.api.TagadaGraph
import picocli.CommandLine
import utils.SolvingRace
import utils.panic
import utils.unreachable
import java.nio.file.Path
import java.util.*
import java.util.concurrent.Callable
import kotlin.system.exitProcess

@CommandLine.Command(name = "tagada-step2")
class Cli : Callable<Int> {
    @CommandLine.Option(names = ["--cipher", "-c"], description = ["The path to the json cipher dag"], required = true)
    val cipher_graph_path: Path? = null

    @CommandLine.Option(
        names = ["--step1", "-s1"],
        description = ["The path to the step1 (json) solutions"],
        required = true
    )
    val step1_path: Path? = null

    @CommandLine.Option(
        names = ["--backend", "-b"],
        description = ["The backend solver (only Choco for now)"]
    )
    val backend: Backend = Backend.Choco

    @CommandLine.Option(
        names = ["--lower-bound", "-lb"],
        description = ["The lb probability of the solution in hundredth of -log2 (e.g. lb = 2^{-3.45} is represented by 345.)"],
        converter = [OptionalIntConverter::class]
    )
    val lb: OptionalInt = OptionalInt.empty()

    @CommandLine.Option(
        names = ["--parallel", "-p"],
        description = ["Enables parallel solving and set the number of available threads"],
        converter = [OptionalIntConverter::class]
    )
    val nThreads: OptionalInt = OptionalInt.empty()

    override fun call(): Int {
        cipher_graph_path ?: unreachable()
        step1_path ?: unreachable()

        val mapper = jacksonObjectMapper()
        val graph = mapper.readValue<TagadaGraph>(cipher_graph_path.toFile())
        if (graph !is Differential) {
            panic("only differential graphs can be used for Step-2 computation")
        }
        if (nThreads != OptionalInt.empty()) {
            //solve multiple solutions in a parallel race.
            val s1List = mapper.readValue<AssignmentList>(step1_path.toFile())
            val outputFile = "results/step2obj" //TODO find a way to properly output solutions
            val buffer = StringBuffer()
            val track = SolvingRace(nThreads.asInt, buffer, outputFile)

            for (s1 in s1List)
                raceBoundedOptimalDifferentialCharacteristic(track, buffer, graph.graph, s1, lb, Choco())
            track.run()

        } else {
            //solve one solution
            val s1 = mapper.readValue<Assignment>(step1_path.toFile())

            val solution = when (backend) {
                Backend.Choco -> boundedOptimalDifferentialCharacteristic(graph.graph, s1, lb, Choco())
            }
            println(mapper.writeValueAsString(solution))
        }
        return 0
    }
}

fun main(args: Array<String>) {
    exitProcess(CommandLine(Cli()).execute(*args))
}