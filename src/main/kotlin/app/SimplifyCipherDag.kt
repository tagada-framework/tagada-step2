package app
/*
import graph.*
import operator.ChocoOperatorInfo
import operator.EqualityOperator
import operator.Operator
import org.chocosolver.solver.Model
import org.chocosolver.solver.Solver
import org.chocosolver.solver.variables.IntVar
import utils.UID
import utils.findById
import utils.panic

class SimplifyCipherDag(

    private val cipherDag: CipherDag,
    private val chocoOp: ChocoOperatorInfo,
    private val step1Sol: Map<UID, Int>,
) {
    data class SBox(val dIn: IntVar, val dOut: IntVar, val p: IntVar)

    private val cachedVars = mutableMapOf<String, IntVar>()
    private val sboxes = mutableListOf<SBox>()

    val inactive =
        mutableMapOf<UID, Int>()//store if each state is active or inactive after step 1 active states propagation.
    val eqtable = mutableMapOf<UID, UID>()//store a list of the equivalents states
    val newstate = mutableListOf<State>()//minimal list of useful states
    val newop = mutableListOf<OperatorNode>()//minimal list of useful operator

    init {


    }
//    val newEqOp = EqualityOperator(Domain(8)) // no need since we unify the equal states

    fun computeEqualnodes() {
        for (state in step1Sol.keys)
            if (step1Sol[state] == 1)
                eqtable.put(state, state)
        for (op in cipherDag.operatorNodes) {
            if (op.uid.startsWith("eq") || op.uid.startsWith("Eq")) {
                val a = op.inputs.first() as UID
                val b = op.outputs.first() as UID
                eqtable.put(a, b)
            } else if ((op.uid.startsWith("xor") || op.uid.startsWith("Xor"))) {
                val opStates = op.inputs + op.outputs
                var dif = 0//number of active states
                for (s in opStates) if (inactive.get(s)!! > 0) dif++
                if (dif == 2) {
                    val a = opStates.find { x -> inactive.get(x)!! > 0 } as UID
                    val b = opStates.findLast { x -> inactive.get(x)!! > 0 } as UID
                    eqtable.put(a, b)
                }
            }
        }
    }

    fun getorigin(state: UID): UID {
        var s = state
        while (eqtable.containsKey(s) && s != eqtable.get(s)) {
//            print(s)
            s = eqtable.get(s) as UID
//            print(" is REPLACED by ")
//            println(s)
        }
        return s
    }

    fun simplifyDag(): CipherDag {
        computeInactive()
        computeEqualnodes()

        val state0 = State("state0", Domain(1), 0)
        newstate.add(state0)

        for (op in cipherDag.operatorNodes) {
            val opStates = op.inputs + op.outputs
            var dif = 0//number of active states
            for (s in opStates) if (inactive.get(s)!! > 0) dif++
            val activestates = opStates.filter { x -> inactive.get(x)!! > 0 }
            for (s in activestates) addState(s)
//            if (dif == 1) {//only add the lonely state and not its useless operator
//            } else if (dif == 2 && (op.uid.startsWith("eq") || op.uid.startsWith("Eq")
//                        || op.uid.startsWith("xor") || op.uid.startsWith("Xor"))
//            ) {//operator is an eq
//            } else
            if (dif > 2 && (op.uid.startsWith("xor") || op.uid.startsWith("Xor"))) {
                val inputs = activestates.filter { x->x!= activestates.last() }.map { x -> getorigin(x) }
                val outputs = getorigin(activestates.last())
                newop.add(OperatorNode(op.uid, inputs, listOf(outputs)))
            } else if (dif > 1 && dif != opStates.size) {
                val inputs = op.inputs.map { x -> if (inactive.get(x)!! > 0) getorigin(x) else state0.uid }
                val outputs = op.outputs.map { x -> if (inactive.get(x)!! > 0) getorigin(x) else state0.uid }
                newop.add(OperatorNode(op.uid, inputs, outputs))
//                print(op.uid)
//                println(" Op INCOMPLETE")
            } else if (dif>0 && dif == opStates.size && !(op.uid.startsWith("eq") || op.uid.startsWith("Eq"))){
                newop.add(op)
            }
        }
        println("Simplification:")
        println("   States "+cipherDag.states.toList().size+" to "+ newstate.toList().size)
        println("   Operators "+cipherDag.operatorNodes.size+" to "+ newop.size)

        return CipherDag(
            newstate.toList(),
//            cipherDag.states.toList(),
            cipherDag.operators,
            newop,
//            cipherDag.operatorNodes,
            cipherDag.plaintext,
            cipherDag.ciphertext
        )
    }

    fun addState(stateUID: UID) {
        val s = getorigin(stateUID)
        val state = cipherDag.states.findById(s)!!//.copy()
        if (!newstate.contains(state)) {
            if (inactive.get(s) == 0)//cosnt values are catched separatelly
                state.domain.max = 0
            else if (inactive.get(s) == 1)
                state.domain.min = 1
            newstate.add(state)
//            print(state)
//            println(" ADD State ")
        }
    }

    fun computeInactive() {
//        val statesUID = cipherDag.states.map { i -> i.uid }
//        val operatorsUID = cipherDag.operatorNodes.map { i -> i.uid }
        for (state in cipherDag.states) {
            val i = state.uid
            val isActive = step1Sol[i]
            if (isActive == 0 || state.value != null)
                inactive.put(i, 0)//pas de dif
            else if (isActive == 1)
                inactive.put(i, 1)//il y a une dif
            else
                inactive.put(i, 2)//on sais pas mais peut être
        }
        var modif = true
        while (modif) {// inactive.containsValue(2)){
            modif = false
            for (o in cipherDag.operatorNodes) {
                if (o.inputs.size == 1 && o.inputs.size == o.outputs.size) {// la diff traverse les op unaires
                    val a = inactive.get(o.inputs[0])!!
                    val b = inactive.get(o.outputs[0])!!
                    if (a < b) {// le 0 est prioritaire sur le 1 ?
                        if (b != 2) {
                            println("FAILURE: incompatible dif, step 1 must have gone wrong")
                        }
                        inactive.replace(o.outputs[0], a)
                        modif = true
                    } else if (b < a) {
                        if (a != 2) {
                            println("FAILURE: incompatible dif, step 1 must have gone wrong")
                        }
                        inactive.replace(o.inputs[0], b)
                        modif = true
                    }
                } else if ((o.inputs.size + o.outputs.size) > 2) {
                    val opStates = o.inputs + o.outputs
                    var nodif = 0
                    var dif = 0
                    var maybe = 0
                    for (s in opStates) {
                        if (inactive.get(s)!! == 0) nodif++
                        else if (inactive.get(s)!! == 1) dif++
                        else if (inactive.get(s)!! == 2) maybe++
                    }
                    if (maybe == 0) {//on vérifie 0 0 1 fail
                        if (dif == 1) {
                            println("FAILURE: a dif is alone, step 1 must have gone wrong")
                        }
                    } else if (maybe == 1) {
                        if (dif == 0) {//0 0 2 --> 0 0 0
                            for (i in o.inputs) inactive.replace(i, 0)
                            for (i in o.outputs) inactive.replace(i, 0)
                            modif = true
                        } else if (dif == 1) {//0 1 2 --> 0 1 1
                            for (i in o.inputs) if (inactive.get(i)!! == 2) inactive.replace(i, 1)
                            for (i in o.outputs) if (inactive.get(i)!! == 2) inactive.replace(i, 1)
                            modif = true
                        }
                    }
                }
            }
        }
//        println(inactive)

//        0 0 1 fail
//        0 0 2 --> 0 0 0
//        0 1 1  Ok (la dif s`annule)
//        0 1 2 --> 0 1 1
//        0 2 2 rien
//        1 1 2 rien
//        1 2 2 rien
//        2 2 2 rien

        //376 168
        //376 168
        //120 8 144
        //225 15

        //warp =0 =1 =2
        //vanilla 140 4 640
        //unary propag 544 16 224
        //nary propag 544 16 224
        //avec les const
        //unary 688 16 80
        //nary 759 22 3
    }
}
////4 parallel modular 2^32 sum over a state
//public static void postSUM32(IntVar[][] S, IntVar[][] S2, IntVar[][] T, Model model) {
//    IntVar v256 = model.intVar("256", 256);
//    for (int j = 0; j < S.length; j++) {
//        IntVar tmp1 = model.intVar("tmp1SUM32", 0, 510);
//        IntVar ret1 = model.intVar("ret1SUM32", 0, 1);
//        model.sum(new IntVar[]{S[0][j], S2[0][j]}, "=", tmp1).post();
//        model.mod(tmp1, 256, T[0][j]).post();
//        model.div(tmp1, v256, ret1).post();
//        IntVar tmp2 = model.intVar("tmp2SUM32", 0, 511);
//        IntVar ret2 = model.intVar("ret2SUM32", 0, 1);
//        model.sum(new IntVar[]{S[1][j], S2[1][j], ret1}, "=", tmp2).post();
//        model.mod(tmp2, 256, T[1][j]).post();
//        model.div(tmp2, v256, ret2).post();
//        IntVar tmp3 = model.intVar("tmp3SUM32", 0, 511);
//        IntVar ret3 = model.intVar("ret3SUM32", 0, 1);
//        model.sum(new IntVar[]{S[2][j], S2[2][j], ret2}, "=", tmp3).post();
//        model.mod(tmp3, 256, T[2][j]).post();
//        model.div(tmp3, v256, ret3).post();
//        IntVar tmp4 = model.intVar("tmp4SUM32", 0, 511);
//        model.sum(new IntVar[]{S[3][j], S2[3][j], ret3}, "=", tmp4).post();
//        model.mod(tmp2, 256, T[3][j]).post();
//    }
//}
//
//public static void postSUM8(IntVar S, IntVar S2, IntVar T, Model model) {
//    IntVar tmp = model.intVar("tmpSUM8", 0, 510);
//    model.sum(new IntVar[]{S, S2}, "=", tmp).post();
//    model.mod(tmp, 256, T).post();
//}*/