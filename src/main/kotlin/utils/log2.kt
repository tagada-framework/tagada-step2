package utils

private fun numBitsSet(x: Int): Int {
    @Suppress("NAME_SHADOWING")
    var x = x
    x -= ((x shr 1) and 0x55555555)
    x = (((x shr 2) and 0x33333333) + (x and 0x33333333))
    x = (((x shr 4) + x) and 0x0f0f0f0f)
    x += (x shr 8)
    x += (x shr 16)
    return x and 0x0000003f
}

fun log2(x: Int): Int {
    @Suppress("NAME_SHADOWING")
    var x = x
    x = x or (x shr 1)
    x = x or (x shr 2)
    x = x or (x shr 4)
    x = x or (x shr 8)
    x = x or (x shr 16)
    return numBitsSet(x) - 1
}