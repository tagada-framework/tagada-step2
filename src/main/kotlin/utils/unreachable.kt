package utils


fun unreachable(): Nothing = throw RuntimeException()