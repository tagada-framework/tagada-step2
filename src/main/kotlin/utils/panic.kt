package utils

fun panic(msg: String? = null): Nothing =
    if (msg != null) throw RuntimeException(msg)
    else throw RuntimeException()