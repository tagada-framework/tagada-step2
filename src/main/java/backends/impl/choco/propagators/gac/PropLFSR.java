package backends.impl.choco.propagators.gac;

import fr.limos.gitlab.decrypt.tagada.structs.specification.LFSR;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableBitSet;

abstract public class PropLFSR extends Propagator<IntVar> {
    private final IntVar input;
    private final IntVar output;
    protected final int WORD_MASK;
    protected final int POLY;
    protected final int NB_BITS;
    protected final int INVERTED_POLY;
    private final IntIterableBitSet ws;

    protected PropLFSR(IntVar input, IntVar output, int nbBits, int poly, int invertedPoly) {
        super(new IntVar[]{input, output}, PropagatorPriority.BINARY, false);
        this.input = input;
        this.output = output;
        this.NB_BITS = nbBits;
        this.POLY = poly;
        this.WORD_MASK = (1 << nbBits) - 1;
        this.INVERTED_POLY = invertedPoly;
        this.ws = new IntIterableBitSet();
    }

    abstract int lfsr(int state);

    abstract int invLfsr(int state);

    protected int xorBits(int state) {
        return xorBits(state, POLY);
    }

    protected int xorBits(int state, int poly) {
        int xor = 0;
        for (int i = 0; i < NB_BITS; i++) {
            if (((state & poly) & (1 << i)) != 0) {
                xor ^= 1;
            }
        }
        return xor;
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        propagateLfsr();
        propagateInvLfsr();
    }

    private void propagateLfsr() throws ContradictionException {
        ws.clear();
        for (int value : input) {
            ws.add(lfsr(value));
        }
        output.removeAllValuesBut(ws, this);
    }

    private void propagateInvLfsr() throws ContradictionException {
        ws.clear();
        for (int value : output) {
            ws.add(invLfsr(value));
        }
        input.removeAllValuesBut(ws, this);
    }

    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            return ESat.eval(lfsr(input.getValue()) == output.getValue());
        }
        return ESat.UNDEFINED;
    }

    public static PropLFSR of(IntVar input, IntVar output, LFSR lfsr) {
        return switch (lfsr.getDirection()) {
            case ShiftLeft -> new PropLFSRLeft(input, output, lfsr.getNbBits(), lfsr.getPoly());
            case ShiftRight -> new PropLFSRRight(input, output, lfsr.getNbBits(), lfsr.getPoly());
        };
    }
}
