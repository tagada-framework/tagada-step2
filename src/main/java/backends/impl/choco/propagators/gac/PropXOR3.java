package backends.impl.choco.propagators.gac;

/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.exception.SolverException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableBitSet;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableSet;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 18/05/2020
 */
public class PropXOR3 extends Propagator<IntVar> {

    static int HIGHEST_BIT = 8;
    static int MAX_VALUE = (int) Math.pow(2, HIGHEST_BIT) - 1;

    IntIterableSet set;

    public PropXOR3(IntVar a, IntVar b, IntVar c) {
        super(new IntVar[]{a, b, c}, PropagatorPriority.TERNARY, false);
        set = new IntIterableBitSet();
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return IntEventType.ALL_EVENTS;
    }

    public void filter(IntVar i, IntVar j, IntVar k) throws ContradictionException {
        if ((i.isInstantiated() && j.isInstantiated()) || i.getDomainSize() * j.getDomainSize() <= k.getDomainSize() * .25) {
            set.clear();
            for (int v1 = i.getLB(); v1 <= i.getUB(); v1 = i.nextValue(v1)) {
                for (int v2 = j.getLB(); v2 <= j.getUB(); v2 = j.nextValue(v2)) {
                    set.add(v1 ^ v2);
                    if (set.size() == MAX_VALUE) {
                        return;
                    }
                }
            }
            k.removeAllValuesBut(set, this);
        }
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        IntVar X = vars[0];
        IntVar Y = vars[1];
        IntVar Z = vars[2];
        int mask;
        mask = 0;
        mask += X.isInstantiated() ? 1 : 0;
        mask += Y.isInstantiated() ? 2 : 0;
        mask += Z.isInstantiated() ? 4 : 0;
        int vx, vy, vz;
        switch (mask) {
            case 0 -> { // nothing instanciated
                filter(X, Y, Z);
                filter(X, Z, Y);
                filter(Y, Z, X);
            }
            case 1 -> { // X is instanciated
                filter(X, Y, Z);
                filter(X, Z, Y);
            }
            case 2 -> { // Y is instanciated
                filter(Y, X, Z);
                filter(Y, Z, X);
            }
            case 4 -> { // Z is instanciated
                filter(Z, X, Y);
                filter(Z, Y, X);
            }
            case 3 -> // X and Y are instanciated
                    filter(X, Y, Z);
            case 5 -> // X and Z are instanciated
                    filter(X, Z, Y);
            case 6 -> // Y and Z are instanciatedvx = X.getValue();
                    filter(Y, Z, X);
            case 7 -> { // X, Y and Z are instanciated
                vx = X.getValue();
                vy = Y.getValue();
                vz = Z.getValue();
                int val = vx ^ vy;
                if (vz != val /*|| val > 255 && vx > 255 || vy > 255*/) {
                    fails();
                }
            }
            default -> throw new SolverException("Unexpected mask " + mask);
        }
    }


    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            int i = vars[0].getValue();
            int j = vars[1].getValue();
            int k = vars[2].getValue();
            return ESat.eval((i ^ j) == k);
        }
        return ESat.UNDEFINED;
    }
}