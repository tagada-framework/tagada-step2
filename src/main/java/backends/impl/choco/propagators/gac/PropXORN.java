package backends.impl.choco.propagators.gac; /**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableBitSet;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableSet;

import java.util.Arrays;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 18/05/2020
 */
public class PropXORN extends Propagator<IntVar> {
    IntIterableSet set;

    public PropXORN(IntVar[] a) {
        super(a, PropagatorPriority.LINEAR, false);
        set = new IntIterableBitSet();
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return IntEventType.ALL_EVENTS;
    }

    private void combiXor(int i, int y, int xor) {
        if (y == i) combiXor(i, y + 1, xor);
        else {
            if (y == vars.length - 1 || (y + 1 == i && y + 1 == vars.length - 1)) {
                for (int v = vars[y].getLB(); v <= vars[y].getUB(); v = vars[y].nextValue(v)) {
                    set.add(xor ^ v);
                }
            } else {
                for (int v = vars[y].getLB(); v <= vars[y].getUB(); v = vars[y].nextValue(v)) {
                    combiXor(i, y + 1, xor ^ v);
                }
            }
        }
    }

    public void filter(int i) throws ContradictionException {
        if (vars.length == 1) {
            vars[0].instantiateTo(0, this);
            return;
        }
        set.clear();
        combiXor(i, 0, 0);
        vars[i].removeAllValuesBut(set, this);
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        boolean filtered = false;//faire les plus gros en premier ?
        for (int i = 0; i < vars.length; i++) {
            if (!vars[i].isInstantiated()) {
                filter(i);
                filtered = true;
            }
        }
        if (!filtered) {
            int x = Arrays.stream(vars).map(IntVar::getValue).reduce(0, (a, b) -> a ^ b);
            if (x != 0) fails();
        }
    }

    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            int x = Arrays.stream(vars).map(IntVar::getValue).reduce(0, (a, b) -> a ^ b);
            return ESat.eval(0 == x);
        }
        return ESat.UNDEFINED;
    }
}