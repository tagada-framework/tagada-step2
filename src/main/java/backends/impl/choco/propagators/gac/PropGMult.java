package backends.impl.choco.propagators.gac;

/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import crypto.GF;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableBitSet;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableSet;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 18/05/2020
 */
public class PropGMult extends Propagator<IntVar> {

    private final IntIterableSet set = new IntIterableBitSet();
    private final GF gf;

    private final IntVar x;

    private final int y;

    private final IntVar z;

    public PropGMult(int poly, IntVar x, int y, IntVar z) {
        super(new IntVar[]{x, z}, PropagatorPriority.BINARY, false);
        gf = GF.Companion.of(poly);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return IntEventType.ALL_EVENTS;
    }

    public void filterMult(IntVar lhs, IntVar res) throws ContradictionException {
        set.clear();
        for (int v1 = lhs.getLB(); v1 <= lhs.getUB(); v1 = lhs.nextValue(v1)) {
            set.add(gf.times(v1, y));
        }
        res.removeAllValuesBut(set, this);
    }

    public void filterDiv(IntVar lhs, IntVar res) throws ContradictionException {
        set.clear();
        for (int v1 = lhs.getLB(); v1 <= lhs.getUB(); v1 = lhs.nextValue(v1)) {
            set.add(gf.div(v1, y));
        }
        res.removeAllValuesBut(set, this);
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        if (y == 0) {
            z.instantiateTo(0, this);
            return;
        }
        if (!x.isInstantiated()) {
            // Z / Y = X
            filterDiv(z, x);
        }
        if (!z.isInstantiated()) {
            // X × Y = Z
            filterMult(x, z);
        }
        if (x.isInstantiated() && z.isInstantiated()) {
            if (gf.times(x.getValue(), y) != z.getValue()) {
                fails();
            }
        }
    }


    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            int i = x.getValue();
            int k = z.getValue();
            return ESat.eval(gf.times(i, y) == k);
        }
        return ESat.UNDEFINED;
    }
}