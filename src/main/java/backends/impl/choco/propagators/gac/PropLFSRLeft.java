package backends.impl.choco.propagators.gac;

import org.chocosolver.solver.variables.IntVar;

public class PropLFSRLeft extends PropLFSR {
    protected PropLFSRLeft(IntVar input, IntVar output, int nbBits, int poly) {
        super(input, output, nbBits, poly, (poly << 1) | (poly & 1));
    }

    @Override
    int lfsr(int state) {
        return ((state << 1) | xorBits(state)) & WORD_MASK;
    }

    @Override
    int invLfsr(int state) {
        return (xorBits(state, INVERTED_POLY) << (NB_BITS - 1)) | (state >> 1);
    }
}
