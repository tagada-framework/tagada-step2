package backends.impl.choco.propagators.fc;

/**
 * Copyright (c) 1999-2020, Ecole des Mines de Nantes
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p>
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of the Ecole des Mines de Nantes nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.exception.SolverException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableBitSet;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableSet;

/**
 * <br/>
 *
 * @author Charles Prud'homme
 * @since 18/05/2020
 */
public class PropXOR3 extends Propagator<IntVar> {

    private IntVar x;
    private IntVar y;
    private IntVar z;

    public PropXOR3(IntVar a, IntVar b, IntVar c) {
        super(new IntVar[]{a, b, c}, PropagatorPriority.TERNARY, false);
        this.x = a;
        this.y = b;
        this.z = c;
    }

    @Override
    public int getPropagationConditions(int vIdx) {
        return IntEventType.ALL_EVENTS;
    }


    @Override
    public void propagate(int evtmask) throws ContradictionException {
        int instantiations = 0;
        if (x.isInstantiated()) instantiations |= 0b100;
        if (y.isInstantiated()) instantiations |= 0b010;
        if (z.isInstantiated()) instantiations |= 0b001;

        switch (instantiations) {
            case 0b000:
            case 0b001:
            case 0b010:
            case 0b100:
                // Do nothing
                break;
            case 0b011:
                if (y.getValue() == 0) break;
                x.instantiateTo(z.getValue() ^ y.getValue(), this);
                break;
            case 0b101:
                if (x.getValue() == 0) break;
                y.instantiateTo(z.getValue() ^ x.getValue(), this);
                break;
            case 0b110:
                z.instantiateTo(x.getValue() ^ y.getValue(), this);
                break;
            case 0b111:
                if ((x.getValue() ^ y.getValue()) != z.getValue()) fails();
                break;
            default:
        }
    }


    @Override
    public ESat isEntailed() {
        if (isCompletelyInstantiated()) {
            int i = vars[0].getValue();
            int j = vars[1].getValue();
            int k = vars[2].getValue();
            return ESat.eval((i ^ j) == k);
        }
        return ESat.UNDEFINED;
    }
}