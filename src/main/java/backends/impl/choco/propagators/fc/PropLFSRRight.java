package backends.impl.choco.propagators.fc;

import org.chocosolver.solver.variables.IntVar;

public class PropLFSRRight extends PropLFSR {
    protected PropLFSRRight(IntVar input, IntVar output, int nbBits, int poly) {
        super(input, output, nbBits, poly, (poly & (1 << (nbBits - 1))) | (poly >> 1));
    }

    @Override
    int lfsr(int state) {
        return (xorBits(state) << (NB_BITS - 1)) | (state >> 1);
    }

    @Override
    int invLfsr(int state) {
        return ((state << 1) | xorBits(state, INVERTED_POLY)) & WORD_MASK;
    }
}
